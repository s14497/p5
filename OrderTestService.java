package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import objects.ClientDetails;
import objects.Order;
import objects.OrderItem;
import service.OrdersService;

public class TestOrderServices {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List <OrderItem> l1 = Arrays.asList(new OrderItem(), new OrderItem(), new OrderItem(), new OrderItem());
		List <OrderItem> l2 = Arrays.asList(new OrderItem(), new OrderItem(), new OrderItem(), new OrderItem()
				, new OrderItem(), new OrderItem(), new OrderItem());
		List <OrderItem> l3 = Arrays.asList(new OrderItem(), new OrderItem(), new OrderItem(), new OrderItem()
				, new OrderItem());
		List <OrderItem> l4 = Arrays.asList(new OrderItem(), new OrderItem(), new OrderItem(), new OrderItem()
				, new OrderItem(), new OrderItem());
		List <OrderItem> l5 = Arrays.asList(new OrderItem(), new OrderItem());
		List <OrderItem> l6 = Arrays.asList(new OrderItem(), new OrderItem(), new OrderItem());
		List <OrderItem> l7 = Arrays.asList(new OrderItem(), new OrderItem(), new OrderItem(), new OrderItem()
				, new OrderItem());
		List <OrderItem> l8 = Arrays.asList(new OrderItem(), new OrderItem(), new OrderItem(), new OrderItem()
				, new OrderItem(), new OrderItem());
		List <OrderItem> l9 = Arrays.asList();
		
		List <Order> list = new ArrayList();
		list.add(new Order(1, new ClientDetails(1, "nick1", "Antoni", "Zalewski", 82), null, l1, "Antek"));
		list.add(new Order(2, new ClientDetails(2, "nick2", "Wiesława", "Bzykała", 61), null, l2, "Aktorzyna"));
		list.add(new Order(3, new ClientDetails(3, "nick3", "Dariusz", "Pałkowski", 28), null, l3, "Pałka"));
		list.add(new Order(4, new ClientDetails(4, "nick4", "Katarzyna", "Figura", 52), null, l4, "Bimbały"));
		list.add(new Order(5, new ClientDetails(5, "nick5", "Janek", "Mąciwoda", 17), null, l5, "Kos"));
		list.add(new Order(6, new ClientDetails(6, "nick6", "Lech", "Struna", 45), null, l6, "Gitara"));
		list.add(new Order(7, new ClientDetails(7, "nick7", "Anna", "Boruta", 47), null, l7, "Zadyma"));
		list.add(new Order(8, new ClientDetails(8, "nick8", "Arkadiusz", "Żniwiarz", 46), null, l8, "Prezes"));
		list.add(new Order(9, new ClientDetails(9, "nick9", "Jakub", "Wasyl", 31), null, l9, "Maluch"));
		
		System.out.println("-------Zad 1-------");
		testFindOrdersWhichHaveMoreThan5OrderItems(list);
		System.out.println("-------Zad 2-------");
		testFindOldestClientAmongThoseWhoMadeOrders(list);
		System.out.println("-------Zad 3-------");
		testFindOrderWithLongestComments(list);
		System.out.println("-------Zad 4-------");
		testGetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(list);
		System.out.println("-------Zad 5-------");
		
		System.out.println("-------Zad 6-------");
		testPrintCapitalizedClientsLoginsWhoHasNameStartingWithS(list);
	}
	
	private static void testFindOrdersWhichHaveMoreThan5OrderItems(List <Order> list){
		
		List <Order> result = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(list);
		
		for(Order order : result){
			
			System.out.println(order.getClientDetails().getLogin());
		}
	}
	
	private static void testFindOldestClientAmongThoseWhoMadeOrders(List <Order> list){
		
		System.out.println(OrdersService.findOldestClientAmongThoseWhoMadeOrders(list).getLogin());
	}
	
	private static void testFindOrderWithLongestComments(List <Order> list){
		
		System.out.println(OrdersService.findOrderWithLongestComments(list).getComments().length());
	}
	
	private static void testGetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List <Order> list){
		
		System.out.println(OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(list));
	}
	
	private static void testPrintCapitalizedClientsLoginsWhoHasNameStartingWithS(List <Order> list){
		
		OrdersService.printCapitalizedClientsLoginsWhoHasNameStartingWithS(list);
	}
}