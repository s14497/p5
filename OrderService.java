package pl.edu.pjatk.mpr.lab5.service;

import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.List;
import java.util.Map;

public class OrdersService {

    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {

    }

    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {

    }

    public static Order findOrderWithLongestComments(List<Order> orders) {

    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {

    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {

    }

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {

    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {

    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        
    }

}
