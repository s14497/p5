package test;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import objects.*;
import service.OrdersService;
import static org.assertj.core.api.Assertions.*;

public class AssertJTests{
	
	// dot. lab5 - daj�c� jako wynik list� zam�wie�, kt�re maj� wi�cej ni� 5 pozycji zamowienia
   	//puste
	@Test
	public void getAndReturnNull_findOrdersWhichHaveMoreThan5OrderItems() {
		List<Order> orders = new ArrayList<>();
		List<Order> ordersWhichHaveMoreThan5OrderItems = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders);
		assertThat(ordersWhichHaveMoreThan5OrderItems).isNull();
	}
	
	// null
	@Test
    public void getAndReturnNull2_findOrdersWhichHaveMoreThan5OrderItems(){
		List<Order> orders = null;
        List<Order> ordersWhichHaveMoreThan5OrderItems = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders);
		assertThat(ordersWhichHaveMoreThan5OrderItems).isNull();
	}
	
	// puste po filtrowaniu
	@Test
    public void getOrdersAndReturnNull_findOrdersWhichHaveMoreThan5OrderItems(){
		List <OrderItem> listOfOrderItems = Arrays.asList(new OrderItem(), new OrderItem());
		List <Order> orders = new ArrayList();
		orders.add(new Order(1, null, null, listOfOrderItems, null));
		orders.add(new Order(2, null, null, listOfOrderItems, null));
		orders.add(new Order(3, null, null, listOfOrderItems, null));
        List <Order> ordersWhichHaveMoreThan5OrderItems = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders);
        assertThat(ordersWhichHaveMoreThan5OrderItems).isNull();
	}
	
	
	// dot. lab5 - daj�c� jako wynik dane klienta, kt�ry jest najstarszy.
	// puste
	@Test
	public void getAndReturnNull_findOldestClientAmongThoseWhoMadeOrders() {
		List<Order> orders = new ArrayList<>();
		ClientDetails oldestClientAmongThoseWhoMadeOrders = OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders);
		assertThat(oldestClientAmongThoseWhoMadeOrders).isNull();
	}

	// null
	@Test
    public void getAndReturnNull2_findOldestClientAmongThoseWhoMadeOrders(){
		List<Order> orders = null;
        ClientDetails oldestClientAmongThoseWhoMadeOrders = OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders);
		assertThat(oldestClientAmongThoseWhoMadeOrders).isNull();
	}
	
	// puste po filtrowaniu
	@Test
    public void getOrdersAndReturnNull_findOldestClientAmongThoseWhoMadeOrders(){
		List <OrderItem> listOfOrderItems = Arrays.asList(new OrderItem(), new OrderItem());
		List <Order> orders = new ArrayList();
		orders.add(new Order(1, null, null, listOfOrderItems, null));
		orders.add(new Order(2, null, null, listOfOrderItems, null));
		orders.add(new Order(3, null, null, listOfOrderItems, null));
        ClientDetails oldestClientAmongThoseWhoMadeOrders = OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders);
        assertThat(oldestClientAmongThoseWhoMadeOrders).isNull();
	}
		
	
	// dot. lab5 - daj�c� jako wynik zam�wienie o najd�u�szym pod wzgl�dem liczby znak�w komentarzu.
	// puste
	@Test
	public void getAndReturnNull_findOrderWithLongestComments() {
		List<Order> orders = new ArrayList<>();
		Order orderWithLongestComments = OrdersService.findOrderWithLongestComments(orders);
		assertThat(orderWithLongestComments).isNull();
	}
	
	// null
	@Test
    public void getAndReturnNull2_findOrderWithLongestComments(){
		List<Order> orders = null;
        Order orderWithLongestComments = OrdersService.findOrderWithLongestComments(orders);
		assertThat(orderWithLongestComments).isNull();
	}

	// puste po filtrowaniu
	@Test
    public void getOrdersAndReturnNull_findOrderWithLongestComments(){
		List <OrderItem> listOfOrderItems = Arrays.asList(new OrderItem(), new OrderItem());
		List <Order> orders = new ArrayList();
		orders.add(new Order(1, null, null, listOfOrderItems, null));
		orders.add(new Order(2, null, null, listOfOrderItems, null));
		orders.add(new Order(3, null, null, listOfOrderItems, null));
        Order orderWithLongestComments = OrdersService.findOrderWithLongestComments(orders);
        assertThat(orderWithLongestComments).isNull();
	}
	    
		
	// dot. lab5 - daj�c� jako wynik napis sk�adaj�cy si� z	imion i	nazwisk	oddzielonych przecinkiem klient�w, kt�rzy maj� wi�cej ni� 18 lat.
    // puste
	@Test
	public void getAndReturnNull_getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld() {
		List<Order> orders = new ArrayList<>();
		String NamesAndSurnamesCommaSeparated = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);
		assertThat(NamesAndSurnamesCommaSeparated).isNull();
	}

	// null
	@Test
    public void getAndReturnNull2_getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(){
		List<Order> orders = null;
        String namesAndSurnamesCommaSeparated = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);
		assertThat(namesAndSurnamesCommaSeparated).isNull();
	}
	
	// puste po filtrowaniu
	@Test
    public void getOrdersAndReturnNull_getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(){
		List <OrderItem> listOfOrderItems = Arrays.asList(new OrderItem(), new OrderItem());
		List <Order> orders = new ArrayList();
		orders.add(new Order(1, null, null, listOfOrderItems, null));
		orders.add(new Order(2, null, null, listOfOrderItems, null));
		orders.add(new Order(3, null, null, listOfOrderItems, null));
        String namesAndSurnamesCommaSeparated = OrdersService.getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);
        assertThat(namesAndSurnamesCommaSeparated).isNull();
	}
	
	
	// dot. lab5 - daj�c� jako wynik posortowan� od �a� do �z� list� nazw pozycji zam�wie�,	kt�rych	komentarz zaczyna si� na liter� �A�.
	// puste
	@Test
	public void getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA() {
		List<Order> orders = new ArrayList<>();
		List<String> orderItemsNamesOfOrdersWithComments = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);
		assertThat(orderItemsNamesOfOrdersWithComments).isNull();
	}
	
	// null
	@Test
    public void getAndReturnNull2_getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(){
		List<Order> orders = null;
        List<String> sortedOrderItemsNamesOfOrdersWithCommentsStartingWithA = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);
		assertThat(sortedOrderItemsNamesOfOrdersWithCommentsStartingWithA).isNull();
	}
	 
	// puste po filtrowaniu 
	@Test
    public void getOrdersAndReturnNull_getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(){
		List <OrderItem> listOfOrderItems = Arrays.asList(new OrderItem(), new OrderItem());
		List <Order> orders = new ArrayList();
		orders.add(new Order(1, null, null, listOfOrderItems, null));
		orders.add(new Order(2, null, null, listOfOrderItems, null));
		orders.add(new Order(3, null, null, listOfOrderItems, null));
        List <String> sortedOrderItemsNamesOfOrdersWithCommentsStartingWithA = OrdersService.getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);
        assertThat(sortedOrderItemsNamesOfOrdersWithCommentsStartingWithA).isNull();
	}
	
	
	// dot. lab5 - daj�c� jako wynik map�, gdzie kluczem jest klient, a	warto�ciami lista zam�wie�, kt�re zosta�y z�o�one przez klienta.
   	// puste
	@Test
	public void getAndReturnNull_groupOrdersByClient(){
		List<Order> orders = new ArrayList<>();
       	Map<ClientDetails, List<Order>> ordersGroupedByClient = OrdersService.groupOrdersByClient(orders);
		assertThat(ordersGroupedByClient).isNull();
	}
		 
	// null
	@Test
    public void getAndReturnNull2_groupOrdersByClient(){
		List<Order> orders = null;
        Map<ClientDetails, List<Order>> ordersGroupedByClient = OrdersService.groupOrdersByClient(orders);
        assertThat(ordersGroupedByClient).isNull();
	}
	
	// puste po filtrowaniu 
	@Test
    public void getOrdersAndReturnNull_groupOrdersByClient(){
		List <OrderItem> listOfOrderItems = Arrays.asList(new OrderItem(), new OrderItem());
		List <Order> orders = new ArrayList();
		orders.add(new Order(1, null, null, listOfOrderItems, null));
		orders.add(new Order(2, null, null, listOfOrderItems, null));
		orders.add(new Order(3, null, null, listOfOrderItems, null));
        Map<ClientDetails, List<Order>> ordersGroupedByClient = OrdersService.groupOrdersByClient(orders);
        assertThat(ordersGroupedByClient).isNull();
	}
	
	
	/* dot. lab5 - daj�c� jako wynik map�, gdzie kluczem jest warto�� logiczna (true lub false),
	a warto�ci� dla false lista klient�w kt�rzy maj� mniej ni� 18 lat, a dla true lista klient�w, kt�rzy maj� 18 lub wi�cej	lat	*/
    // puste
	@Test
    public void getAndReturnNull_partitionClientsByUnderAndOver18(){
		List<Order> orders = new ArrayList<>();
        Map<Boolean, List<ClientDetails>> partitionedClientByUnderAndOrver18 = OrdersService.partitionClientsByUnderAndOver18(orders);
        assertThat(partitionedClientByUnderAndOrver18).isNull();
	}
	
	// null
	@Test
    public void getAndReturnNull2_partitionClientsByUnderAndOver18(){
		List<Order> orders = null;
        Map<Boolean, List<ClientDetails>> partitionedClientByUnderAndOrver18 = OrdersService.partitionClientsByUnderAndOver18(orders);
        assertThat(partitionedClientByUnderAndOrver18).isNull();
	}
	
	// puste po filtrowaniu 
	@Test
    public void getOrdersAndReturnNull_partitionClientsByUnderAndOver18(){
		List <OrderItem> listOfOrderItems = Arrays.asList(new OrderItem(), new OrderItem());
		List <Order> orders = new ArrayList();
		orders.add(new Order(1, null, null, listOfOrderItems, null));
		orders.add(new Order(2, null, null, listOfOrderItems, null));
		Map<Boolean, List<ClientDetails>> partitionedClientByUnderAndOrver18 = OrdersService.partitionClientsByUnderAndOver18(orders);
        assertThat(partitionedClientByUnderAndOrver18).isNull();
	}
}
	