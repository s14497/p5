package pl.edu.pjatk.mpr.lab5.service;

import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.List;
import java.util.Map;

public class OrdersService {
	
	
	// dającą jako wynik listę zamówień, które mają więcej niż 5 pozycji zamowienia
    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
		
    	
    return orders.stream()
    	.filter(o -> o.getItems().size() > 5)
    	.collect(Collectors.toList());
    }


	// dającą jako wynik dane klienta, który jest najstarszy.
    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
    
     
	ClientDetails oldest = orders.stream()
	.map(Order::getClientDetails)
	.max(Comparator.comparing(ClientDetails::getAge))
	.get();
	return oldest;
    }

	
	// dającą jako wynik zamówienie o najdłuższym pod względem liczby znaków komentarzu.
    public static Order findOrderWithLongestComments(List<Order> orders) {
		
	//Strumienie
	Order result = orders.stream()
       .max(Comparator.comparing(order -> order.getComments().length()))
       .get();
       return result;
    }

    
	// dającą jako wynik napis składający się z	imion i	nazwisk	oddzielonych przecinkiem klientów, którzy mają więcej niż 18 lat.
    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
	
	//Strumienie
	return orders.stream()
	.filter (order -> order.getClientDetails().getAge() > 18)
	.map (Order::getClientDetails)
	.map (ClientDetails::toString)
	.collect(Collectors.joining(","));
    }

	
	// dającą jako wynik posortowaną od ‘a’ do ‘z’ listę nazw pozycji zamówień,	których	komentarz zaczyna się na literę ‘A’.
    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {

	//Strumienie
	List<String> ordersItemsNamesfromOrderWithCommentA = orders.stream()
                .filter(order -> order.getComments().startsWith("A"))
                .flatMap(order -> order.getItems().stream())
                .map(item -> item.getName())
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        return ordersItemsNamesfromOrderWithCommentA;
	
	
	// wypisującą na ekran loginy klientów, których	imię zaczyna się na literę ‘S’
    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {

	//Strumienie 
	orders.stream()
    		.map(Order::getClientDetails)
    		.filter(order -> order.getName().toUpperCase().startsWith("S"))
    		.map(ClientDetails::getLogin)
    		.forEach(System.out::println);
    }
    
	
	// dającą jako wynik mapę, gdzie kluczem jest klient, a	wartościami lista zamówień, które zostały złożone przez klienta.
    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {

	Map<ClientDetails, List<order>> ordersByClient = orders.stream()
			.collect(Collectors.groupingBy(order::getClientDetails));

        return ordersByClient;
    }
    
	
	/* dającą jako wynik mapę, gdzie kluczem jest wartość logiczna (true lub false),
	a wartością dla false lista klientów którzy mają mniej niż 18 lat, a dla true lista klientów, którzy mają 18 lub więcej	lat	*/
    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        
    Map<Boolean, List<clientDetails>> clientsUnderAndOver18 = orders.stream()
            .map(order -> order.getClientDetails())
            .distinct()
            .collect(Collectors.partitioningBy(clientDetails -> clientDetails.getAge() >= 18));

        return clientsUnderAndOver18;
    }

}
